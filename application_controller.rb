# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  before_filter :allow_iframe_requests
    def allow_iframe_requests
    response.headers.delete('X-Frame-Options')
  end
  include Authentication
  protect_from_forgery with: :exception
end
