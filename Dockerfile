FROM davefrancis/dradis-ce:v1.0
RUN apt update && apt install -y supervisor
RUN rm -r /var/lib/apt/lists/*

WORKDIR /home/dradis-ce

COPY ./Gemfile.plugins /home/dradis-ce/ 
#COPY ./application.rb /home/dradis-ce/config/environments/ 
COPY ./supervisord.conf /home/dradis-ce/
COPY ./application_controller.rb /home/dradis-ce/app/controllers/
COPY ./development.rb /home/dradis-ce/config/environments/

# left for backward compatibility
COPY ./development.sqlite3 /home/dradis-ce/db/

COPY ./development.sqlite3 /.backup/dradis-ce/db/
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN bundle install

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-c", "/home/dradis-ce/supervisord.conf"]
