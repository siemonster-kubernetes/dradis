#!/bin/bash

if [ ! -f "/home/dradis-ce/db/development.sqlite3" ]; then
    mkdir -p /home/dradis-ce/db
    cp -p /.backup/dradis-ce/db/development.sqlite3 /home/dradis-ce/db/development.sqlite3
fi

exec "$@"
